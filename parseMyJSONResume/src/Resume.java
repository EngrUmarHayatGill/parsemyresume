import java.util.ArrayList;



public class Resume {
	private Basics basics;
	private ArrayList<Work> work;
	private ArrayList<Volunteer> volunteer;
	private ArrayList<Education> education;
	private ArrayList<Awards> awards;
	private ArrayList<Publication> publications;
	private ArrayList<Skill> skills;
	private ArrayList<Language> languages;
	private ArrayList<Interest> interests;
	private ArrayList<Reference> references;
	public ArrayList<Awards> getAwards() {
		return awards;
	}
	public void setAwards(ArrayList<Awards> awards) {
		this.awards = awards;
	}
	public Basics getBasics() {
		return basics;
	}
	public void setBasics(Basics basics) {
		this.basics = basics;
	}
	public ArrayList<Education> getEducation() {
		return education;
	}
	public void setEducation(ArrayList<Education> education) {
		this.education = education;
	}
	public ArrayList<Interest> getInterests() {
		return interests;
	}
	public void setInterests(ArrayList<Interest> interests) {
		this.interests = interests;
	}
	public ArrayList<Language> getLanguages() {
		return languages;
	}
	public void setLanguages(ArrayList<Language> languages) {
		this.languages = languages;
	}
	public ArrayList<Publication> getPublications() {
		return publications;
	}
	public void setPublications(ArrayList<Publication> publications) {
		this.publications = publications;
	}
	public ArrayList<Reference> getReferences() {
		return references;
	}
	public void setReferences(ArrayList<Reference> references) {
		this.references = references;
	}
	public ArrayList<Skill> getSkills() {
		return skills;
	}
	public void setSkills(ArrayList<Skill> skills) {
		this.skills = skills;
	}
	public ArrayList<Volunteer> getVolunteer() {
		return volunteer;
	}
	public void setVolunteer(ArrayList<Volunteer> volunteer) {
		this.volunteer = volunteer;
	}
	public ArrayList<Work> getWork() {
		return work;
	}
	public void setWork(ArrayList<Work> work) {
		this.work = work;
	}
	


}
